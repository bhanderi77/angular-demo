// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  LOGIN_API_ENDPOINT: 'http://localhost:3000/api/v1/auth/login',
  LOGOUT_API_ENDPOINT: 'http://localhost:3000/api/v1/auth/logout',
  APPLICATION_ENDPOINT: 'http://localhost:3000/api/v1/application',
  ADDRESS_ENDPOINT: 'http://localhost:3000/api/v1/address',
  INVESTOR_ENDPOINT: 'http://localhost:3000/api/v1/investor',
  MASTER_DATA_ENDPOINT:
    'https://www.mykshitij.com/SWIFTWEBAPIUAT/api/global/masterdata',
  GENERATE_OTP_API_ENDPOINT: 'http://localhost:3000/api/v1/auth/generate-otp',
  VERIFY_OTP_API_ENDPOINT: 'http://localhost:3000/api/v1/auth/verify-otp',
  INVESTMENT_ENDPOINT: 'http://localhost:3000/api/v1/investment',
  BANK_DETAILS_ENDPOINT: 'http://localhost:3000/api/v1/bank-details',
  ADDRESS_DETAILS_ENDPOINT: 'http://localhost:3000/api/v1/address',
  ADD_REMARK_ENDPOINT: 'http://localhost:3000/api/v1/application/add-remark',
  ESIGN_ENDPOINT: 'http://localhost:3000/api/v1/esign',
  UPLOAD_DOC_ENDPOINT: 'http://localhost:3000/api/v1/document',
  DOC_URL: '/uploads',
  IFSC_DETAILS_ENDPOINT:
    'https://www.mykshitij.com/SWIFTWEBAPIUAT/api/onboardinvestor/getbankdetailsbyifsccode',
  GET_KYC_DETAILS_ENDPOINT:
    'http://localhost:3000/api/v1/application/get-ckyc-details',
  REMITTENCE_ENDPOINT: 'http://localhost:3000/api/v1/remittence',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
