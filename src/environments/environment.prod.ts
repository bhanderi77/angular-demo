export const environment = {
  production: true,
  LOGIN_API_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/auth/login',
  LOGOUT_API_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/auth/logout',
  APPLICATION_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/application',
  ADDRESS_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/address',
  INVESTOR_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/investor',
  MASTER_DATA_ENDPOINT:
    'https://www.mykshitij.com/SWIFTWEBAPIUAT/api/global/masterdata',
  GENERATE_OTP_API_ENDPOINT:
    'http://http://13.234.107.76:3000/api/v1/auth/generate-otp',
  VERIFY_OTP_API_ENDPOINT:
    'http://http://13.234.107.76:3000/api/v1/auth/verify-otp',
  INVESTMENT_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/investment',
  BANK_DETAILS_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/bank-details',
  ADDRESS_DETAILS_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/address',
  ADD_REMARK_ENDPOINT:
    'http://http://13.234.107.76:3000/api/v1/application/add-remark',
  ESIGN_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/esign',
  UPLOAD_DOC_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/document',
  DOC_URL: '/uploads',
  IFSC_DETAILS_ENDPOINT:
    'https://www.mykshitij.com/SWIFTWEBAPIUAT/api/onboardinvestor/getbankdetailsbyifsccode',
  GET_KYC_DETAILS_ENDPOINT:
    'http://http://13.234.107.76:3000/api/v1/application/get-ckyc-details',
  REMITTENCE_ENDPOINT: 'http://http://13.234.107.76:3000/api/v1/remittence',
};

// https://gis.edelweissfin.com:3000
// http://http://13.234.107.76
