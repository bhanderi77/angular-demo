import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'app-form2',
  templateUrl: './form2.component.html',
  styleUrls: ['./form2.component.scss'],
})
export class Form2Component implements OnInit {
  number: number;
  numberOfChanges = 0;

  constructor(private readonly commonService: CommonService) {}

  ngOnInit() {
    this.commonService.number.subscribe(data => {
      this.number = data;

      this.numberOfChanges += 1;
    });
  }
}
