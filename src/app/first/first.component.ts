import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin, from, of, Subject, throwError } from 'rxjs';
import { catchError, retry, switchMap, takeUntil } from 'rxjs/operators';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'app-first',
  templateUrl: './first.component.html',
  styleUrls: ['./first.component.scss'],
})
export class FirstComponent implements OnInit {
  param = '';

  numbers = [2, 5, 78, 823, 1];
  number: number;
  isNotDestroyed: Subject<Boolean> = new Subject();
  constructor(
    private route: ActivatedRoute,
    private readonly commonService: CommonService
  ) {}

  ngOnInit() {
    this.param = this.route.snapshot.params.name;

    this.createObservable().subscribe({
      next: data => {
        console.log(data);
      },
    });

    this.commonService.number
      .pipe(takeUntil(this.isNotDestroyed))
      .subscribe(data => {
        console.log(data);
        this.number = data;
      });
  }

  callAPis() {
    forkJoin({
      op1: this.api1Call(),
      op2: this.api2Call(),
      op3: this.api3Call(),
    })
      .pipe(switchMap(({ op1, op2, op3 }): any => {}))
      .subscribe({
        error: err => {},
      });
  }

  createObservable() {
    // from() operator is used to create observable from array, iterable , object , promise
    return from(this.numbers).pipe(
      switchMap(number => {
        number = number * 2;

        return of(number);
      }),
      catchError(err => {
        return throwError('');
      })
    );
  }

  api1Call() {
    return of(true).pipe(
      retry(2),
      catchError(err => {
        return throwError('err in api 1');
      })
    );
  }

  api2Call() {
    return of(true).pipe(
      catchError(err => {
        return throwError('err in api 2');
      })
    );
  }

  api3Call() {
    return of(true).pipe(
      catchError(err => {
        return throwError('err in api 3');
      })
    );
  }

  updateValue() {
    this.commonService.number.next(this.number + 1);
  }
}
