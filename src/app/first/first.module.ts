import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FirstRoutingModule } from './first-routing.module';
import { FirstComponent } from './first.component';
import { Form1Component } from './form1/form1.component';
import { Form2Component } from './form2/form2.component';

@NgModule({
  declarations: [FirstComponent, Form1Component, Form2Component],
  imports: [CommonModule, FirstRoutingModule],
})
export class FirstModule {}
