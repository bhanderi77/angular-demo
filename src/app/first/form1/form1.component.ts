import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'app-form1',
  templateUrl: './form1.component.html',
  styleUrls: ['./form1.component.scss'],
})
export class Form1Component implements OnInit {
  number: number;

  constructor(private readonly commonService: CommonService) {}

  ngOnInit() {
    this.commonService.number.subscribe(data => {
      this.number = data;
    });
  }
}
