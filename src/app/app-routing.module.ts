import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  // {
  //   path: 'loggedout',
  //   loadChildren: () =>
  //     import('./investor-ui/logout/logout.module').then(
  //       m => m.LogoutPageModule
  //     ),
  // },
  // { path: '', redirectTo: '/home', pathMatch: 'full' },
  // { path: '**', redirectTo: '' },
  {
    path: 'profile/:name',
    loadChildren: () => import('./first/first.module').then(m => m.FirstModule),
    canActivate: [AuthGuard],
  },
  {
    path: 'second',
    loadChildren: () =>
      import('./second/second.module').then(m => m.SecondModule),
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
