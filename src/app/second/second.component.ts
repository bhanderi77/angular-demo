import { Component, OnInit } from '@angular/core';
import { CommonService } from 'src/services/common.service';

@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.scss'],
})
export class SecondComponent implements OnInit {
  number = 0;
  constructor(private readonly commonService: CommonService) {}

  ngOnInit() {
    this.commonService.number.subscribe(data => {
      this.number = data;
    });
  }

  updateValue() {
    this.commonService.number.next(this.number + 1);
  }
}
